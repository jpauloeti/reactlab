import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute, Link, browserHistory, Redirect } from 'react-router'
import routes from './config/routes';

import App from './components/app';
import CardDetail from './components/card_detail';

ReactDOM.render((
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={App} />
    </Route>
    <Route path="card/:id" component={App} />
  </Router>
), document.querySelector('.app'))
