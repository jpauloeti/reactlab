import React, { Component } from 'react';
import Columns from './columns'
import _ from 'lodash'
import rest from 'rest'
import q from 'q'

const API_KEY = "b585d29650fb547bde43e0b626f74087";
const TOKEN = "ea06b2e8e4aa22840a46d222beed88b290906a60df5d70b55a9c5e38181f5896";
const BOARD = 'e45QSSE6';

class App extends Component {
  constructor(props){
    super(props);

    this.state = {
      columns: [],
      cardDetail: []
    };

    this.loadLists()

  }
  componentDidMount(){
    if(!_.isEmpty(this.props.params) && this.props.params.id){
      this.setState({cardDetail:this.props.params.id})
    }
  }

  // TODO: verificar rota ao carregar component e mostrar o card se for o card

  loadLists() {
    rest(`https://api.trello.com/1/boards/${BOARD}?fields=id,name,idOrganization,dateLastActivity&lists=all&list_fields=id,name&key=${API_KEY}&token=${TOKEN}`).then(response => {
      let res = JSON.parse(response.entity)
      this.loadCards(res.lists)
    });
  }

  loadCards(lists) {
    let promises = [];
    _.forEach(lists, list => {
      let restCall = rest(`https://api.trello.com/1/lists/${list.id}/cards?fields=id,name,badges,labels`).then(response => {
        let res = JSON.parse(response.entity)
        list.cards = res;
      })
      promises.push(restCall);
    })
    q.all(promises).then( ()=>{
      this.setState({columns:lists})
    });
  }


  render() {
    return (
      <div>
        <div className="row page-header">Trecco</div>
        <div className="row page-container">
          <Columns
            columns={this.state.columns}
            cardDetail={this.state.cardDetail} />
        </div>
      </div>
    );
  }
}

export default App
