import React, { Component } from 'react';
import _ from 'lodash'
import { browserHistory } from 'react-router'
import ReactDOM from 'react-dom';
import CardDetail from './card_detail';

class CardsItem extends Component {
  constructor(props){
    super(props);
  }

  componentWillMount(){
    if(this.props.cardDetail && this.props.card.id == this.props.cardDetail){
      this.gotoCard()
    }
  }

  gotoCard() {
    browserHistory.push(`/card/${this.props.card.id}`);

    ReactDOM.render((
      <CardDetail 
        details={this.props.card} 
        colName={this.props.colName} 
      />
    ), document.querySelector('.card_detail'))
  }

  render() {
    let labels = this.props.card.labels.map(label => {
      return <span style={{"backgroundColor": label.color}} key={label.id}></span>
    })

    return (
      <a href="javascript:void(0)" onClick={() => { this.gotoCard() }} className="card">
        { labels.length > 0 && <div className="labels">{labels}</div> }
        {this.props.card.name}
      </a>
    )
  }
}

export default CardsItem
