import React, { Component } from 'react';
import _ from 'lodash'
import CardsItem from './cards_item'

class Cards extends Component {
  constructor(props){
    super(props);
  }

  render() {
    let cards = this.props.cards.map(card => {
      return <CardsItem
          card={card}
          key={card.id} 
          cardDetail={this.props.cardDetail}
          colName={this.props.colName}
          />
    })

    return (
      <div>
        {cards}
      </div>
    )
  }
}

export default Cards
