import React, { Component } from 'react';
import _ from 'lodash'
import Cards from './cards'

class ColumnsItem extends Component {
  constructor(props){
    super(props);
  }

  render() {
    return (
      <div className="col-md-2 column">
        <h5>{this.props.column.name}</h5>
        <Cards
          cards={this.props.column.cards}
          cardDetail={this.props.cardDetail}
          colName={this.props.column.name}
          />

        <a href="javascript:void(0);" className="add-a-card">Add a card. . .</a>
      </div>
    )
  }
}

export default ColumnsItem
