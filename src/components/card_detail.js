import React, { Component } from 'react';
import { browserHistory } from 'react-router'
import ReactDOM from 'react-dom';

class CardDetail extends Component {
  constructor(props){
    super(props);

    this.state = {
      visible: true
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      visible: true
    })
  }

  handleClose() {
    this.setState({
      visible: false
    })
    browserHistory.push('/')
  }

  render() {
    if (this.state.visible === false) {
      return null;
    }

    return (
      <div>
        <div className="card-detail-modal col-md-6" role="document">
          <div className="content">
            <div className="header">
              <h5 className="title">{this.props.details.name} <br/><span className="list">in list <u>{this.props.colName}</u></span></h5>
              <button type="button" className="close" onClick={() => this.handleClose()} data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <hr />
            <div className="body">
              {this.props.details.name}
            </div>
          </div>
        </div>
        <div className="backdrop" onClick={() => this.handleClose()}></div>
      </div>
    )
  }

}

export default CardDetail
