import React, { Component } from 'react';
import ColumnsItem from './columns_item'
import _ from 'lodash'

class Columns extends Component {
  constructor(props){
    super(props)
  }


  render() {
    let lists = this.props.columns.map( col => {
      return <ColumnsItem column={col} key={col.id} cardDetail={this.props.cardDetail} />
    })

    return (
      <div>
        {lists}
      </div>
    )
  }
}

export default Columns
