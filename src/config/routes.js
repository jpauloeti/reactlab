import React from 'react';
import App from '../components/app';
import { Router, Route, DefaultRoute } from 'react-router';

export default (
  <Route name="app" path="/" handler={App}>

  </Route>
);
